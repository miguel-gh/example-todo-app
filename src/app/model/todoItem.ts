export class TodoItem {
    constructor(
        public description?: string,
        public status?: string
    ) {}
}
