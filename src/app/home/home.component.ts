import { Component, OnInit } from '@angular/core';

import { TodoItem } from '../model/todoItem';

@Component({
  selector: 'todo-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  todoList: Array<TodoItem>;
  todoItem: TodoItem;

  constructor() {
    this.todoList = [];
    this.todoItem = new TodoItem('', 'todo');
  }

  ngOnInit() {
  }

  addItemToList() {
    this.todoList.push(this.todoItem);
    console.log('Added: ' + JSON.stringify(this.todoItem));
    this.todoItem = new TodoItem('', 'todo');
  }

  removeItem(item: TodoItem) {
    console.log('Deleted: ' + JSON.stringify(item));
    const index = this.todoList.indexOf(item);
    if (index > -1) {
      this.todoList.splice(index, 1);
    }
  }

  doItem(item: TodoItem) {
    item.status = item.status === 'todo' ? 'done' : 'todo';
    console.log('Status Change: ' + JSON.stringify(item));
  }

  checkDoneStatus(item: TodoItem) {
    return item.status === 'done';
  }

  isListDone() {
    let isAllDone = false;
    if (this.todoList.length > 0) {
      isAllDone = this.todoList.every(this.checkDoneStatus);
      console.log('is list done?: ' + isAllDone);
    }
    return isAllDone;
  }

}
