import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { TodoItem } from '../model/todoItem';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppComponent } from '../app.component';
import { FormsModule } from '@angular/forms';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule ],
      declarations: [ AppComponent, HomeComponent ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add a todo Item', () => {
    component.todoItem = new TodoItem('Test Item', 'todo');
    component.addItemToList();
    expect(component.todoList.length).toBeGreaterThan(0);
  });

  it('should remove a todo Item', () => {
    const theItem = new TodoItem('Test Item', 'todo');
    component.todoItem = theItem;
    component.addItemToList();
    expect(component.todoList.length).toBeGreaterThan(0);

    component.removeItem(theItem);
    expect(component.todoList.length).toBe(0);
  });

  it('should change status of todo Item', () => {
    const theItem = new TodoItem('Test Item', 'todo');
    component.todoItem = theItem;
    component.addItemToList();
    expect(component.todoList.length).toBeGreaterThan(0);

    component.doItem(theItem);
    expect(component.todoList[0].status).toBe('done');
  });

});
